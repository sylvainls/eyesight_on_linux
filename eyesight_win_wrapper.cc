#include <string>
#include <thread>
#include <filesystem>
#include <cstdlib>
#include <iostream>

int main( int argc, char** argv )
{
    using namespace std::chrono_literals;

    std::cout << "== Eyesight native wrapper / Windows part ==" << std::endl;

    // find output image filename
    const std::string OUT = "--output";
    std::string outputfile = "";
    const int max = argc - 1;
    for ( int i = 1; i < max; ++i ) {
        if ( OUT.compare( argv[i] ) == 0 ) {
            outputfile = argv[i+1];
            break;
        }
    }
    std::cout << "File to render: " << outputfile << std::endl;

    // collect and protect parameters
    std::string params = "eyesight_native_wrapper.exe";
    for ( int i = 1; i < argc; ++i ) {
        params += " \"";
        params += argv[i];
        params += "\"";
    }
    // start the native wrapper
    std::cout << "Starting " << params << std::endl;
    std::system( params.c_str() );
    // on Wine, because eyesight_native_wrapper.exe is actually a shell
    // script, system returns immediately, so we now wait for the output file
    // to exist before quitting
    std::cout << "Waiting for " << outputfile << "..." << std::endl;
    while (!std::filesystem::exists( outputfile )) {
        std::this_thread::sleep_for( 1000ms );
    }
    std::cout << "Okay!" << std::endl;
    return 0;
}
