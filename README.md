# Studio On Linux

This repository contains procedures and useful files to build and use BrickLink Studio’s Photorealistic renderer, Eyesight, on Linux.

———
LEGO and BrickLink are trademarks of the LEGO Groud, which does not sponsor,
endorse, or authorize this.
